# Practice
* We have established our own project on GitLab website
  ![](https://gitlab.com/picturenick2/forpicgo/uploads/e11cf70429352fb48931cb8f3506da82/GitLabweb.png)

* Basically learn to use GitBash for Pull or Push files
  ![](https://gitlab.com/picturenick2/forpicgo/uploads/fa2c52340f64cdc2f814acf8b1c3c956/git.png)

* Use VS code software and Markown programming language to write web pages
  ![](https://gitlab.com/picturenick2/forpicgo/uploads/530df1c72d7fc529c034e0b54dfeb383/markdom.png)

* Edit and organize pictures with PicGO.